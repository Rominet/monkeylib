package com.cryocrystal.monkey.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.cryocrystal.monkey.R;
import com.cryocrystal.monkey.statics.IntentUtils;


/**
 * Created by t.destribats on 07/07/2016.
 */
public class KeyImageView extends ImageView implements View.OnClickListener, View.OnLongClickListener {

    private static LocalBroadcastManager broadcastManager;

    public KeyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (broadcastManager == null) {
            broadcastManager = LocalBroadcastManager.getInstance(context);
        }
        init(context, attrs);
    }

    protected void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MonKeyViews, 0, 0);
        getId();

        a.recycle();
        setOnClickListener(this);
        setOnLongClickListener(this);
    }


    @Override
    public void onClick(View v) {
        IntentUtils.sendPressedIntent(broadcastManager, getId(), false);
    }

    @Override
    public boolean onLongClick(View view) {
        IntentUtils.sendPressedIntent(broadcastManager, getId(), true);
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP){
            IntentUtils.sendLongPressEndedIntent(broadcastManager, getId());
        }
        return super.onTouchEvent(event);
    }

}