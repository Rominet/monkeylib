package com.cryocrystal.monkey.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.LayoutRes;
import android.support.annotation.StyleRes;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.cryocrystal.monkey.R;
import com.cryocrystal.monkey.activity.KeyboardActivity;
import com.cryocrystal.monkey.statics.MonKeyConfig;
import com.cryocrystal.monkey.statics.Utils;


/**
 * Created by t.destribats on 06/07/2016.
 */
public class CustomEditText extends EditText {

    public enum PushMode {
        NONE, AUTO, ALIGNED, ALWAYS;

        public static PushMode fromOrdinal(int ordinal) {
            PushMode[] values = values();
            if (ordinal < 0 || ordinal >= values.length) {
                return AUTO;
            }
            return values[ordinal];
        }
    }

    private static InputMethodManager imm;

    private
    @LayoutRes
    int keyboardLayout;
    private PushMode pushMode = PushMode.AUTO;
    private long keyboardAnimationDuration = MonKeyConfig.ANIMATION_DURATION;
    private
    @StyleRes
    int keyboardAnimationStyle = R.style.KeyboardAnimations;

    public CustomEditText(Context context, @LayoutRes int keyboardLayout) {
        super(context);
        init();
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        importAttrs(context, attrs);
        init();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        importAttrs(context, attrs);
        init();
    }

    private void init() {
        imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (keyboardLayout == 0) {
            throw new IllegalArgumentException("CustomEditText must have a valid keyboardLayout attribute");
        }

        setTextIsSelectable(true);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                openKeyboardAndPlaceFocus();
            }
        });
        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    openKeyboardAndPlaceFocus();
                } else {
                    closeKeyboard();
                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setShowSoftInputOnFocus(false);
        }
    }

    private void importAttrs(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MonKeyViews, 0, 0);
        keyboardLayout = a.getResourceId(R.styleable.MonKeyViews_keyboardLayout, 0);
        pushMode = PushMode.fromOrdinal(a.getInt(R.styleable.MonKeyViews_pushMode, PushMode.AUTO.ordinal()));
        keyboardAnimationDuration = a.getInteger(R.styleable.MonKeyViews_keyboardAnimDuration, (int) MonKeyConfig.ANIMATION_DURATION);
        keyboardAnimationStyle = a.getResourceId(R.styleable.MonKeyViews_keyboardAnimationStyle, R.style.KeyboardAnimations);

        a.recycle();
    }

    private void openKeyboardAndPlaceFocus() {
        Activity activity = Utils.activityFromContext(getContext());
        if (activity instanceof KeyboardActivity) {
            ((KeyboardActivity) activity).showKeyboard(this);
        }
    }

    private void closeKeyboard() {
        Activity activity = Utils.activityFromContext(getContext());
        if (activity instanceof KeyboardActivity) {
            ((KeyboardActivity) activity).hideKeyboard();
        }
    }

    public
    @LayoutRes
    int getKeyboardLayout() {
        return keyboardLayout;
    }

    public void setKeyboardLayout(@LayoutRes int keyboardLayout) {
        this.keyboardLayout = keyboardLayout;
    }

    public PushMode getPushMode() {
        return pushMode;
    }

    public long getKeyboardAnimationDuration() {
        return keyboardAnimationDuration;
    }

    public void setKeyboardAnimationDuration(long keyboardAnimationDuration) {
        this.keyboardAnimationDuration = keyboardAnimationDuration;
    }

    public
    @StyleRes
    int getKeyboardAnimationStyle() {
        return keyboardAnimationStyle;
    }

    public void setKeyboardAnimationStyle(@StyleRes int keyboardAnimationStyle) {
        this.keyboardAnimationStyle = keyboardAnimationStyle;
    }
}