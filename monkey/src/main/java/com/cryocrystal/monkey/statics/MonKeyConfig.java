package com.cryocrystal.monkey.statics;

/**
 * Created by t.destribats on 08/07/2016.
 */
public class MonKeyConfig {
    public static final String KEY_PRESSED_INTENT = "MonKeyTypedIntent";
    public static final String KEY_TYPED_ID = "MonKeyID";
    public static final String KEY_TYPED_VALUE = "MonKeyValue";
    public static final String KEY_LONG_CLICK = "MonKeyLongClick";
    public static final long ANIMATION_DURATION = 200;
    public static final String KEY_RELEASED_INTENT = "MonkeyReleasedIntent";
    public static final int MAX_TRY_ON_SHOW_POPUP = 3;
}
