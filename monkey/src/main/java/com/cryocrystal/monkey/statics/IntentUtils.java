package com.cryocrystal.monkey.statics;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

/**
 * Created by t.destribats on 25/07/2016.
 */
public class IntentUtils {

    public static void sendPressedIntent(LocalBroadcastManager broadcastManager, int id, String value, boolean longClicked) {
        Intent i = new Intent(MonKeyConfig.KEY_PRESSED_INTENT);
        if (longClicked) {
            i.putExtra(MonKeyConfig.KEY_LONG_CLICK, true);
        }
        if (id != View.NO_ID) {
            i.putExtra(MonKeyConfig.KEY_TYPED_ID, id);
        }
        if (value != null) {
            i.putExtra(MonKeyConfig.KEY_TYPED_VALUE, value);
        }
        broadcastManager.sendBroadcast(i);
    }

    public static void sendPressedIntent(LocalBroadcastManager broadcastManager, int id, boolean longClicked) {
        sendPressedIntent(broadcastManager, id, null, longClicked);
    }

    public static void sendLongPressEndedIntent(LocalBroadcastManager broadcastManager, int id){
        Intent i = new Intent(MonKeyConfig.KEY_RELEASED_INTENT);
        if (id != View.NO_ID) {
            i.putExtra(MonKeyConfig.KEY_TYPED_ID, id);
        }
        broadcastManager.sendBroadcast(i);
    }
}
