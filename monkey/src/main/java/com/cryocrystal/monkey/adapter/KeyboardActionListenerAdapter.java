package com.cryocrystal.monkey.adapter;

import android.app.Activity;
import android.os.Handler;
import android.text.Editable;
import android.util.SparseArray;
import android.view.View;
import android.widget.EditText;

import com.cryocrystal.monkey.listener.OnKeyboardActionListener;

/**
 * Created by t.destribats on 08/07/2016.
 */
public class KeyboardActionListenerAdapter implements OnKeyboardActionListener {

    private Activity activity;
    private final Handler handler = new Handler();
    private final SparseArray<Boolean> pressedKeys = new SparseArray<>();;

    public KeyboardActionListenerAdapter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onKey(int code) {

    }

    @Override
    public void onLongClickedKey(int code) {
        onKey(code);
    }

    @Override
    public void onKey(String value) {
        View focusCurrent = activity.getWindow().getCurrentFocus();
        if (focusCurrent == null || !(focusCurrent instanceof EditText)) {
            return;
        }
        EditText edittext = (EditText) focusCurrent;
        Editable editable = edittext.getText();

        int start = edittext.getSelectionStart();
        editable.insert(start, value);
    }

    @Override
    public void onLongClickedKey(String value) {
        onKey(value);
    }

    public EditText getCurrentEditText() {
        View focusCurrent = activity.getWindow().getCurrentFocus();
        if (focusCurrent == null || !(focusCurrent instanceof EditText)) {
            return null;
        }
        return (EditText) focusCurrent;
    }

    public void deleteLastCharacter() {
        EditText edittext = getCurrentEditText();
        if (edittext == null) {
            return;
        }

        Editable editable = edittext.getText();
        int start = edittext.getSelectionStart();
        if (editable != null && start > 0) {
            editable.delete(start - 1, start);
        }
    }

    public void deleteLongClicked(final int code) {
        listenForKeyReleased(code);

        Runnable progressiveEraser = new Runnable() {
            public int step = 0;

            @Override
            public void run() {
                if (pressedKeys.get(code) == null){
                    return;
                }

                EditText edittext = getCurrentEditText();
                if (edittext == null) {
                    return;
                }

                Editable editable = edittext.getText();
                int start = edittext.getSelectionStart();
                if (editable != null && start > 0) {
                    editable.delete(start - 1, start);
                    handler.postDelayed(this, 300 - step * 50);
                }
                step++;
            }
        };

        handler.post(progressiveEraser);
    }

    public void listenForKeyReleased(int code){
        pressedKeys.put(code, true);
    }

    public void onLongPressEnded(int code){
        pressedKeys.remove(code);
    }
}
