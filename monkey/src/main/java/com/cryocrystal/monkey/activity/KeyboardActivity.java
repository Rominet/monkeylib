package com.cryocrystal.monkey.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.PopupWindow;

import com.cryocrystal.monkey.R;
import com.cryocrystal.monkey.adapter.KeyboardActionListenerAdapter;
import com.cryocrystal.monkey.statics.MonKeyConfig;
import com.cryocrystal.monkey.statics.Utils;
import com.cryocrystal.monkey.widget.CustomEditText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public abstract class KeyboardActivity extends AppCompatActivity {

    private final KeyboardInfo keyboardInfo = new KeyboardInfo();
    @LayoutRes
    int lastKeyboardLayout = 0;
    private LocalBroadcastManager localBroadcastManager;
    private KeyboardActionListenerAdapter keyboardActionListenerAdapter;
    private PopupWindow keyboardWindow;
    private int keyboardHeight;
    private int screenHeight;
    private boolean recreateKeyboard = true;
    private List<Runnable> pendingRunnables;

    private BroadcastReceiver keyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras == null) {
                return;
            }
            if (MonKeyConfig.KEY_PRESSED_INTENT.equals(intent.getAction())) {
                boolean longClicked = extras.getBoolean(MonKeyConfig.KEY_LONG_CLICK, false);
                if (extras.containsKey(MonKeyConfig.KEY_TYPED_ID)) {
                    if (longClicked) {
                        keyboardActionListenerAdapter.onLongClickedKey(intent.getExtras().getInt(MonKeyConfig.KEY_TYPED_ID));
                    } else {
                        keyboardActionListenerAdapter.onKey(intent.getExtras().getInt(MonKeyConfig.KEY_TYPED_ID));
                    }
                }
                if (extras.containsKey(MonKeyConfig.KEY_TYPED_VALUE)) {
                    if (longClicked) {
                        keyboardActionListenerAdapter.onLongClickedKey(intent.getExtras().getString(MonKeyConfig.KEY_TYPED_VALUE));
                    } else {
                        keyboardActionListenerAdapter.onKey(intent.getExtras().getString(MonKeyConfig.KEY_TYPED_VALUE));
                    }
                }
            } else /* if (MonKeyConfig.KEY_RELEASED_INTENT.equals(intent.getAction()) */ {
                if (extras.containsKey(MonKeyConfig.KEY_TYPED_ID)) {
                    keyboardActionListenerAdapter.onLongPressEnded(intent.getExtras().getInt(MonKeyConfig.KEY_TYPED_ID));
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setKeyboardActionListenerAdapter(new KeyboardActionListenerAdapter(this));
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        pendingRunnables = new ArrayList<>();

        changeHeights();
    }

    public void setKeyboardActionListenerAdapter(KeyboardActionListenerAdapter keyboardActionListenerAdapter) {
        this.keyboardActionListenerAdapter = keyboardActionListenerAdapter;
    }

    protected void changeHeights() {
        screenHeight = Utils.getScreenHeight(this);
        keyboardHeight = (int) (screenHeight * 0.45);
        recreateKeyboard = true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        changeHeights();
        if (isCustomKeyboardVisible()) {
            hideKeyboard();

            View focusedView = getCurrentFocus();
            if (focusedView instanceof CustomEditText){
                showKeyboard((CustomEditText) focusedView);
            }
        }
    }

    public void showKeyboard(final CustomEditText compoundView) {
        if (compoundView == null) {
            hideKeyboard();
            return;
        }

        if (!recreateKeyboardIfNeeded(compoundView.getKeyboardLayout()) && isCustomKeyboardVisible()) {
            return;
        }

        if (!compoundView.hasFocus()) {
            compoundView.requestFocus();
        }

        compoundView.postDelayed(new Runnable() {
            @Override
            public void run() {
                animateWindowAndKeyboard(compoundView);
            }
        }, 10);
    }

    private boolean recreateKeyboardIfNeeded(@LayoutRes int keyboardLayout) {
        if (recreateKeyboard || keyboardLayout != lastKeyboardLayout) {
            hideKeyboard();
            createKeyboard(keyboardLayout);
            lastKeyboardLayout = keyboardLayout;
            return true;
        }
        return false;
    }

    private void animateWindowAndKeyboard(CustomEditText compoundView) {
        CustomEditText.PushMode pushMode = compoundView.getPushMode();
        keyboardInfo.windowView = ((ViewGroup) compoundView.getRootView()).getChildAt(0);
        keyboardInfo.viewPushed = false;
        keyboardInfo.handler = compoundView.getHandler();
        keyboardInfo.animationDuration = compoundView.getKeyboardAnimationDuration();
        keyboardWindow.setAnimationStyle(compoundView.getKeyboardAnimationStyle());

        int translationY = 0;
        if (pushMode != CustomEditText.PushMode.NONE) {
            translationY = translationToBeAligned(compoundView);
            switch (pushMode) {
                case AUTO:
                    translationY = translationY > 0 ? keyboardHeight : 0;
                    break;
                case ALWAYS:
                    translationY = keyboardHeight;
                    keyboardInfo.viewPushed = true;
                    break;
            }
            keyboardInfo.viewPushed = translationY > 0;
        }

        if (keyboardInfo.viewPushed) {
            keyboardInfo.windowView.animate()
                    .translationY(-translationY)
                    .setInterpolator(new DecelerateInterpolator())
                    .setDuration(keyboardInfo.animationDuration)
                    .start();
        }

        showPopup(0);
    }

    private void showPopup(final int nbTry) {
        if (keyboardInfo.handler == null || keyboardWindow == null){
            return;
        }
        try {
            keyboardWindow.showAtLocation(keyboardInfo.windowView, Gravity.BOTTOM, 0, 0);
        } catch (WindowManager.BadTokenException ignored) {
            if (nbTry + 1 < MonKeyConfig.MAX_TRY_ON_SHOW_POPUP){
                Runnable retryRunnable = new Runnable() {
                    @Override
                    public void run() {
                        showPopup(nbTry + 1);
                    }
                };
                pendingRunnables.add(retryRunnable);
                keyboardInfo.handler.postDelayed(retryRunnable,200);
            }
        }
    }

    private void createKeyboard(@LayoutRes int keyboardLayout) {
        View keyboard = getLayoutInflater().inflate(keyboardLayout, null);
        if (recreateKeyboard) {
            if (keyboardInfo.handler != null){
                for (Runnable run : pendingRunnables){
                    keyboardInfo.handler.removeCallbacks(run);
                }
                pendingRunnables.clear();
            }
            keyboardWindow = new PopupWindow(keyboard, ViewGroup.LayoutParams.MATCH_PARENT, keyboardHeight, false);
            keyboardWindow.setAnimationStyle(R.style.KeyboardAnimations);
            recreateKeyboard = false;
        } else {
            keyboardWindow.setContentView(keyboard);
        }
    }

    private int translationToBeAligned(View compoundView) {
        int[] location = new int[2];
        compoundView.getLocationOnScreen(location);

        int[] rootLocation = new int[2];
        ((ViewGroup) compoundView.getRootView()).getChildAt(0).getLocationOnScreen(rootLocation);

        return Math.max(0, keyboardHeight - (screenHeight - (location[1] - rootLocation[1] + compoundView.getHeight())));
    }

    @Override
    public void onBackPressed() {
        if (isCustomKeyboardVisible()) {
            hideKeyboard();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        localBroadcastManager.unregisterReceiver(keyReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter broadcastFilter = new IntentFilter();
        broadcastFilter.addAction(MonKeyConfig.KEY_PRESSED_INTENT);
        broadcastFilter.addAction(MonKeyConfig.KEY_RELEASED_INTENT);
        localBroadcastManager.registerReceiver(keyReceiver, broadcastFilter);
    }

    public boolean isCustomKeyboardVisible() {
        return keyboardWindow != null && keyboardWindow.isShowing();
    }

    public void hideKeyboard() {
        if (!isCustomKeyboardVisible()) {
            return;
        }

        if (keyboardInfo.viewPushed) {
            keyboardInfo.windowView.animate()
                    .translationY(0)
                    .setInterpolator(new AccelerateInterpolator())
                    .setDuration(keyboardInfo.animationDuration)
                    .start();
        }
        keyboardWindow.dismiss();
    }

    private final static class KeyboardInfo {
        public View windowView;
        public boolean viewPushed;
        public long animationDuration;
        public Handler handler;
    }
}
