package com.cryocrystal.monkey.listener;

/**
 * Created by t.destribats on 08/07/2016.
 */
public interface OnKeyboardActionListener {
    void onKey(int code);
    void onLongClickedKey(int code);
    void onKey(String value);
    void onLongClickedKey(String value);
}
