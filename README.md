MonKey
======

Simple way to add custom keyboards to specific EditText.

Download
--------

Configure your module-level `build.gradle` to include jCenter and dependencies:

```groovy
buildscript {
  repositories {
    jCenter()
   }
}
  dependencies {
    compile  'com.cryocrystal.monkey:monkey:0.3.4'
  }

```

How to
------

If your activity needs a custom keyboard start by extending KeyboardActivity

```java
class ExampleActivity extends KeyboardActivity {
  ...
}
```


#### CustomEditText

Add it in your layout. (Activity or Fragment)

```xml
<com.cryocrystal.monkey.widget.CustomEditText
  android:id="@+id/isbn_edittext"
  app:keyboardLayout="@layout/keyboard_keys_other"
  android:layout_width="match_parent"
  android:layout_height="wrap_content"/>
```

The attribute `keyboardLayout` is mandatory.

You can also use :
* `pushMode="none"` in order to never shift the view upward.
* `pushMode="always"` to do the opposite
* `pushMode="auto"` _(default)_ only shift the view if it's under the keyboard.
* `pushMode="aligned"` to shift the view so the EditText is just above the keyboard.

`keyboardAnimDuration` to set your own duration on the keyboard animations.

`keyboardAnimationStyle="@style/MyKeyboardAnims"` if you want to specify your own animations. (As you would for a Dialog)

Here is the default style :

```xml
<style name="KeyboardAnimations">
  <item name="android:windowEnterAnimation">@anim/push_bottom_in</item>
  <item name="android:windowExitAnimation">@anim/push_bottom_out</item>
</style>
```


#### Keyboard Layout

You can use a fully custom layout or use key helper from the library.

Here is an example.

```xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.v7.widget.GridLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/grey"
    app:columnCount="4"
    app:rowCount="4">

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="1"
        app:layout_column="0"
        app:layout_columnWeight="1"
        app:layout_row="0"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="2"
        app:layout_column="1"
        app:layout_columnWeight="1"
        app:layout_row="0"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="3"
        app:layout_column="2"
        app:layout_columnWeight="1"
        app:layout_row="0"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyImageView
        android:id="@+id/button_keyboard_clear"
        android:scaleType="center"
        android:src="@drawable/icon_delete_clavier"
        app:layout_column="3"
        app:layout_columnWeight="1"
        app:layout_row="0"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="4"
        app:layout_column="0"
        app:layout_columnWeight="1"
        app:layout_row="1"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="5"
        app:layout_column="1"
        app:layout_columnWeight="1"
        app:layout_row="1"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="6"
        app:layout_column="2"
        app:layout_columnWeight="1"
        app:layout_row="1"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="7"
        app:layout_column="0"
        app:layout_columnWeight="1"
        app:layout_row="2"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="8"
        app:layout_column="1"
        app:layout_columnWeight="1"
        app:layout_row="2"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="9"
        app:layout_column="2"
        app:layout_columnWeight="1"
        app:layout_row="2"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="-"
        app:layout_column="0"
        app:layout_columnWeight="1"
        app:layout_row="3"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="0"
        app:layout_column="1"
        app:layout_columnWeight="1"
        app:layout_row="3"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        app:keyValue="X"
        app:layout_column="2"
        app:layout_columnWeight="1"
        app:layout_row="3"
        app:layout_rowWeight="1"/>

    <com.cryocrystal.monkey.widget.KeyTextView
        android:id="@+id/button_keyboard_ok"
        android:text="OK"
        android:layout_width="0dp"
        app:layout_column="3"
        app:layout_columnWeight="1"
        app:layout_row="1"
        app:layout_rowSpan="3"
        app:layout_rowWeight="1"/>

</android.support.v7.widget.GridLayout>
```

#### KeyTextView - KeyImageView

__KeyTextView__ has a `keyValue` attribute which is the value that will be added
to the EditText when the key is pressed. And you can make it different than it's text
by specifying one. _android:text="something"_

Both __KeyTextView__ and __KeyImageView__ can have `ids` so you can implement your own actions.
You can also use the predefined method to clear your text. _(Similar to the default keyboard)_ See the example below.

```java
class ExampleActivity extends KeyboardActivity {
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_example);

      setKeyboardActionListenerAdapter(new KeyboardActionListenerAdapter(this) {
	    @Override
        public void onKey(int code) {
          switch (code) {
            case R.id.button_keyboard_ok:
              hideKeyboard();
              break;
            case R.id.button_clear:
              deleteLastCharacter();
              break;
            default:
              super.onKey(code);
              break;
          }
        }

        @Override
        public void onLongClickedKey(int code) {
          switch (code){
            case R.id.button_keyboard_clear:
              deleteLongClicked(code);
              break;
            default:
              super.onLongClickedKey(code);
          }
        }
      });
    }
	
	...
  
}
```



License
-------

    Copyright 2016 Thierry Destribats

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.