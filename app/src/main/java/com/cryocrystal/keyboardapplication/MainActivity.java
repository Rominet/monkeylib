package com.cryocrystal.keyboardapplication;

import android.app.AlertDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.cryocrystal.monkey.activity.KeyboardActivity;
import com.cryocrystal.monkey.widget.CustomEditText;

import butterknife.ButterKnife;

/**
 * Created by t.destribats on 06/07/2016.
 */
public class MainActivity extends KeyboardActivity {

    ImageButton clearButton;
    Button okButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }
}
