package com.cryocrystal.keyboardapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.cryocrystal.monkey.activity.KeyboardActivity;
import com.cryocrystal.monkey.adapter.KeyboardActionListenerAdapter;

import butterknife.ButterKnife;

/**
 * Created by t.destribats on 08/07/2016.
 */
public class FragmentActivity extends KeyboardActivity {

    Button alertButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        alertButton = ButterKnife.findById(this, R.id.alert_button);

        setKeyboardActionListenerAdapter(new KeyboardActionListenerAdapter(this) {
            @Override
            public void onKey(int code) {
                switch (code) {
                    case R.id.button_keyboard_clear:
                        deleteLastCharacter();
                        break;
                    case R.id.button_keyboard_ok:
                        hideKeyboard();
                        break;
                    default:
                        super.onKey(code);
                        break;
                }
            }

            @Override
            public void onLongClickedKey(int code) {
                switch (code){
                    case R.id.button_keyboard_clear:
                        deleteLongClicked(code);
                        break;
                    default:
                        super.onLongClickedKey(code);
                }
            }
        });

        alertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyUberDialog uberDialog = new MyUberDialog();
                uberDialog.show(getSupportFragmentManager(), "myUberTruc");
            }
        });
    }
}
